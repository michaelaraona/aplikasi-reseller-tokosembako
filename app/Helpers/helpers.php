<?php

use Carbon\Carbon;

function tanggal($date)
{
    $return = Carbon::parse($date)->format('d-m-Y');
    return $return;
}
