<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\LogLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('admin')->check()) {
            return redirect()->route('admin.dashboard.index');
            // dd(auth()->guard('admin')->user());
        }
        return view('admin.login.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        $cek = Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password]);
        if ($cek) {
            LogLogin::create([
                'log_browser' => $request->header('User-Agent'),
                'log_ip_browser' => $request->ip(),
                'is_logged' => 1,
                'id_user' => auth()->guard('admin')->user()->id_admin,
                'session_user' => 1,
            ]);

            return redirect()->route('admin.dashboard.index');
        } else {
            LogLogin::create([
                'log_browser' => $request->header('User-Agent'),
                'log_ip_browser' => $request->ip(),
                'is_logged' => 2,
                'id_user' => null,
                'session_user' => 1,
            ]);
            return redirect()->back()->with(['jenis' => 'error', 'pesan' => 'Email atau Password salah']);
        }
    }

    public function logout(Request $request)
    {

        LogLogin::create([
            'log_browser' => $request->header('User-Agent'),
            'log_ip_browser' => $request->ip(),
            'is_logged' => 3,
            'id_user' => auth()->guard('admin')->user()->id_admin,
            'session_user' => 1,
        ]);

        Auth::guard('admin')->logout();

        return redirect()->route('admin.login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
