<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\Unique;

class KategoriController extends Controller
{
    protected $dataView;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataView['kategori'] = Kategori::all();
        return view('admin.kategori.index', $dataView);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            ['nama' => 'required|max:40|unique:kategori,nama',]
        );

        try {
            Log::info('Request simpan kategori = ' . json_encode($request->all()));
            Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
            Log::info('Start');

            DB::beginTransaction();
            $kategori =  Kategori::create([
                'nama' => ucfirst($request->nama),
            ]);

            Log::info("Data Kategori Baru = " . json_encode($kategori));

            DB::commit();

            return redirect()->route('admin.kategori.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil menambahkan kategori']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error simpan data kategori = " . $exception->getMessage());
            Log::error("Error simpan data kategori = " . $exception->getFile());
            Log::error("Error simpan data kategori = " . $exception->getTraceAsString());

            return redirect()->route('admin.kategori.index')->with(['jenis' => 'error', 'pesan' => 'Gagal menambahkan kategori']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataView['kategori'] = Kategori::find($id);
        return view('admin.kategori.edit', $dataView);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kategori = Kategori::find($id);

        $request->validate([
            'nama' => "required|max:40|unique:kategori,nama,$kategori->id_kategori,id_kategori",
        ]);

        try {
            Log::info('Request update data kategori berisi = ' . json_encode($kategori));
            Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
            Log::info('Start');

            DB::beginTransaction();
            $kategori->update([
                'nama' => ucfirst($request->nama),
            ]);

            Log::info("Data kategori setelah diupdate = " . json_encode($kategori));

            DB::commit();

            return redirect()->route('admin.kategori.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil update kategori']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error update data kategori = " . $exception->getMessage());
            Log::error("Error update data kategori = " . $exception->getFile());
            Log::error("Error update data kategori = " . $exception->getTraceAsString());

            return redirect()->route('admin.kategori.index')->with(['jenis' => 'error', 'pesan' => 'Gagal update kategori']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = Kategori::find($id);
        Log::info('Request data kategori yang ingin didelete = ' . json_encode($kategori));
        Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
        Log::info('Start');

        $kategori->delete();

        Log::info('Data kategori berhasil di delete');

        return redirect()->route('admin.kategori.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil Delete kategori']);
    }
}
