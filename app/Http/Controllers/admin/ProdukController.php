<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Kategori;
use App\Models\Produk;
use App\Models\StokProduk;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProdukController extends Controller
{
    protected $dataView;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataView['produk'] = Produk::all();
        return view('admin.produk.index', $dataView);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dataView = [
            'kategori' => Kategori::all(),
            'supplier' => Supplier::all(),
        ];

        return view('admin.produk.create', $dataView);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'kode' => 'required|unique:produk,kode_produk|max:20',
                'nama' => 'required|max:100',
                'kategori_id' => 'required',
                'supplier_id' => 'required',
            ]
        );

        try {
            Log::info('Request simpan produk = ' . json_encode($request->all()));
            Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
            Log::info('Start');

            DB::beginTransaction();
            $produk =  Produk::create([
                'kode_produk' => strtoupper($request->kode),
                'nama' => ucwords($request->nama),
                'kategori_id' => $request->kategori_id,
                'supplier_id' => $request->supplier_id,
                'deskripsi' => ucfirst($request->deskripsi),
            ]);

            Log::info("Data Produk Baru = " . json_encode($produk));

            DB::commit();

            return redirect()->route('admin.produk.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil menambahkan produk']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error simpan data produk = " . $exception->getMessage());
            Log::error("Error simpan data produk = " . $exception->getFile());
            Log::error("Error simpan data produk = " . $exception->getTraceAsString());

            return redirect()->route('admin.produk.index')->with(['jenis' => 'error', 'pesan' => 'Gagal menambahkan produk']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataView['produk'] = Produk::find($id);
        $dataView['stokProduk'] = StokProduk::where('produk_id', $id)->get();
        return view('admin.produk.show', $dataView);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataView = [
            'produk' => Produk::find($id),
            'kategori' => Kategori::all(),
            'supplier' => Supplier::all(),
        ];

        return view('admin.produk.edit', $dataView);
    }

    public function editStok($stokID, $produkID)
    {
        $dataView['produk'] = Produk::Find($produkID);
        $dataView['stok'] = StokProduk::find($stokID);
        return view('admin.produk.editStok', $dataView);
    }

    public function updateStok(Request $request, $id)
    {
        $stok = StokProduk::find($id);

        $request->validate(
            [
                'stok' => "required|numeric",
            ]
        );

        try {
            $idProduk = $request->produk_id;
            Log::info('Request update data stok berisi = ' . json_encode($stok));
            Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
            Log::info('Start');

            DB::beginTransaction();
            $stok->update([
                'stok_barang' => $request->stok,
            ]);

            Log::info("Data stok setelah diupdate = " . json_encode($stok));
            Log::info("Keterangan update stok = " . $request->keterangan);

            DB::commit();

            return redirect()->route('admin.produk.show', $idProduk)->with(['jenis' => 'success', 'pesan' => 'Berhasil update stok']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error update data stok = " . $exception->getMessage());
            Log::error("Error update data stok = " . $exception->getFile());
            Log::error("Error update data stok = " . $exception->getTraceAsString());

            return redirect()->route('admin.produk.editStok', ['stokID' => $id, 'produkID' => $idProduk])->with(['jenis' => 'error', 'pesan' => 'Gagal update stok']);
        }
    }

    public function editHarga($id, $idProduk)
    {
        $dataView['produk'] = Produk::Find($idProduk);
        // $dataView['harga'] = Harga::find($id);
        return view('admin.produk.editHarga', $dataView);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produk = Produk::find($id);

        $request->validate(
            [
                'kode' => "required|unique:produk,kode_produk,$produk->id_produk,id_produk|max:20",
                'nama' => 'required|max:100',
                'kategori_id' => 'required',
                'supplier_id' => 'required',
            ]
        );

        try {
            Log::info('Request update data produk berisi = ' . json_encode($produk));
            Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
            Log::info('Start');

            DB::beginTransaction();
            $produk->update([
                'kode_produk' => strtoupper($request->kode),
                'nama' => ucwords($request->nama),
                'kategori_id' => $request->kategori_id,
                'supplier_id' => $request->supplier_id,
                'deskripsi' => ucfirst($request->deskripsi),
            ]);

            Log::info("Data produk setelah diupdate = " . json_encode($produk));

            DB::commit();

            return redirect()->route('admin.produk.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil update produk']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error update data produk = " . $exception->getMessage());
            Log::error("Error update data produk = " . $exception->getFile());
            Log::error("Error update data produk = " . $exception->getTraceAsString());

            return redirect()->route('admin.produk.index')->with(['jenis' => 'error', 'pesan' => 'Gagal update produk']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::find($id);
        Log::info('Request data produk yang ingin didelete = ' . json_encode($produk));
        Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
        Log::info('Start');

        $produk->delete();

        Log::info('Data produk berhasil di delete');

        return redirect()->route('admin.produk.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil Delete produk']);
    }
}
