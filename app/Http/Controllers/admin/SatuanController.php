<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Satuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SatuanController extends Controller
{
    protected $dataView;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataView['satuan'] = Satuan::all();
        return view('admin.satuan.index', $dataView);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.satuan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required|max:10|unique:satuan,nama',
                'keterangan' => 'max:25'
            ]
        );

        try {
            Log::info('Request simpan satuan = ' . json_encode($request->all()));
            Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
            Log::info('Start');

            DB::beginTransaction();
            $satuan =  Satuan::create([
                'nama' => strtoupper($request->nama),
                'keterangan' => ucfirst($request->keterangan ?? '-'),
            ]);

            Log::info("Data Satuan Baru = " . json_encode($satuan));

            DB::commit();

            return redirect()->route('admin.satuan.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil menambahkan satuan']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error simpan data satuan = " . $exception->getMessage());
            Log::error("Error simpan data satuan = " . $exception->getFile());
            Log::error("Error simpan data satuan = " . $exception->getTraceAsString());

            return redirect()->route('admin.satuan.index')->with(['jenis' => 'error', 'pesan' => 'Gagal menambahkan satuan']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataView['satuan'] = Satuan::find($id);
        return view('admin.satuan.edit', $dataView);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $satuan = Satuan::find($id);

        $request->validate([
            'nama' => "required|max:10|unique:satuan,nama,$satuan->id_satuan,id_satuan",
            'keterangan' => 'max:25',
        ]);

        try {
            Log::info('Request update data satuan berisi = ' . json_encode($satuan));
            Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
            Log::info('Start');

            DB::beginTransaction();
            $satuan->update([
                'nama' => strtoupper($request->nama),
                'keterangan' => ucfirst($request->keterangan ?? '-'),
            ]);

            Log::info("Data satuan setelah diupdate = " . json_encode($satuan));

            DB::commit();

            return redirect()->route('admin.satuan.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil update satuan']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error update data satuan = " . $exception->getMessage());
            Log::error("Error update data satuan = " . $exception->getFile());
            Log::error("Error update data satuan = " . $exception->getTraceAsString());

            return redirect()->route('admin.satuan.index')->with(['jenis' => 'error', 'pesan' => 'Gagal update satuan']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $satuan = Satuan::find($id);
        Log::info('Request data satuan yang ingin didelete = ' . json_encode($satuan));
        Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
        Log::info('Start');

        $satuan->delete();

        Log::info('Data satuan berhasil di delete');

        return redirect()->route('admin.satuan.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil delete satuan']);
    }
}
