<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Produk;
use App\Models\Satuan;
use App\Models\StokProduk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class StokProdukController extends Controller
{
    protected $dataView;
    // $table->foreignId('produk_id');
    // $table->foreignId('satuan_id');
    // $table->bigInteger('stok_barang');
    // $table->string('image')->nullable();

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataView = [
            'stok' => StokProduk::get(),
        ];

        return view('admin.stokProduk.index', $dataView);
    }

    public function getSatuanByProdukJson($id_produk)
    {
        $satuan = Satuan::whereNotIn('id_satuan', function ($q) use ($id_produk) {
            $q->select('satuan_id')
                ->from(with(new StokProduk)->getTable())
                ->where('produk_id', $id_produk);
        })->get();

        return response()->json($satuan);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $id_produk = request('id_produk');
        // $dataView['produk'] = Produk::Find($id_produk);

        // $dataView['satuan'] = Satuan::whereNotIn('id_satuan', function ($q) use ($id_produk) {
        //     $q->select('satuan_id')
        //         ->from(with(new StokProduk)->getTable())
        //         ->where('produk_id', $id_produk);
        // })->get();
        $status = collect([
            [
                'no' => '0',
                'kondisi' => 'Bagus',
            ],
            [
                'no' => '1',
                'kondisi' => 'Expired',
            ],
            [
                'no' => '2',
                'kondisi' => 'Rusak',
            ],
        ]);

        $dataView = [
            'produk' => Produk::get(),
            'satuan' => Satuan::get(),
            'status' => $status,
        ];

        return view('admin.stokProduk.create', $dataView);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate(
            [
                'produk' => 'required',
                'image' => 'required',
                'image' => 'image|file|max:1024',
                'stok' => 'required',
                'satuan' => 'required',
                'status' => 'required',
                'harga' => 'required',
            ]
        );

        try {
            Log::info('Request simpan stok produk = ' . json_encode($request->all()));
            Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
            Log::info('Start');

            if ($request->file('image')) {
                $validateData['image'] = $request->file('image')->store('gambar-produk');
            }

            DB::beginTransaction();
            $stok =  StokProduk::create([
                'produk_id' => $validateData['produk'],
                'image' => $validateData['image'],
                'stok_barang' => $validateData['stok'],
                'satuan_id' => $validateData['satuan'],
                'status' => $validateData['status'],
            ]);

            Log::info("Data Stok Produk Baru = " . json_encode($stok));

            DB::commit();

            return redirect()->route('admin.stokProduk.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil menambahkan stok produk']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error simpan data stok produk = " . $exception->getMessage());
            Log::error("Error simpan data stok produk = " . $exception->getFile());
            Log::error("Error simpan data stok produk = " . $exception->getTraceAsString());

            return redirect()->route('admin.stokProduk.index')->with(['jenis' => 'error', 'pesan' => 'Gagal menambahkan stok produk']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataView['stok'] = StokProduk::find($id);
        $dataView['satuan'] = Satuan::get();
        return view('admin.stokProduk.edit', $dataView);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(StokProduk $stokProduk)
    {
        if ($stokProduk->image) {
            Storage::delete($stokProduk->image);
        }

        Log::info('Request data stok produk yang ingin didelete = ' . json_encode($stokProduk));
        Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
        Log::info('Start');

        StokProduk::destroy($stokProduk->id_stokProduk);

        Log::info('Data stok produk berhasil di delete');

        return redirect()->route('admin.stokProduk.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil Delete Stok Produk']);
    }
}
