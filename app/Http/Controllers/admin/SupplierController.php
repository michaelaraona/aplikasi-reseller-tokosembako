<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Produk;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SupplierController extends Controller
{
    protected $dataView;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataView['supplier'] = Supplier::all();
        return view('admin.supplier.index', $dataView);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            [
                'nama' => 'required|max:40|unique:supplier,nama',
                'notelp' => 'max:13|unique:supplier,notelp',
                'email' => 'max:50|unique:supplier,email',
                'npwp' => 'max:30|unique:supplier,npwp',
            ]
        ];

        if ($request->email != NULL) {
            $rules['email'] = 'max:50|unique:supplier,email|email:dns';
        }

        $request->validate($rules);

        try {
            Log::info('Request simpan supplier = ' . json_encode($request->all()));
            Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
            Log::info('Start');

            DB::beginTransaction();
            $supplier =  Supplier::create([
                'nama' => strtoupper($request->nama),
                'email' => $request->email,
                'notelp' => $request->notelp,
                'npwp' => $request->npwp,
                'alamat' => $request->alamat,
            ]);

            Log::info("Data Supplier Baru = " . json_encode($supplier));

            DB::commit();

            return redirect()->route('admin.supplier.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil menambahkan supplier']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error simpan data supplier = " . $exception->getMessage());
            Log::error("Error simpan data supplier = " . $exception->getFile());
            Log::error("Error simpan data supplier = " . $exception->getTraceAsString());

            return redirect()->route('admin.supplier.index')->with(['jenis' => 'error', 'pesan' => 'Gagal menambahkan supplier']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataView['supplier'] = Supplier::find($id);
        $dataView['produk'] = Produk::where('supplier_id', $id)->get();
        return view('admin.supplier.show', $dataView);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataView['supplier'] = Supplier::find($id);
        return view('admin.supplier.edit', $dataView);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::find($id);

        $rules = [
            [
                'nama' => "required|max:40|unique:supplier,nama,$supplier->id_supplier,id_supplier",
                'notelp' => "max:13|unique:supplier,notelp,$supplier->id_supplier,id_supplier",
                'email' => "max:50|unique:supplier,email,$supplier->id_supplier,id_supplier",
                'npwp' => "max:30|unique:supplier,npwp,$supplier->id_supplier,id_supplier",
            ]
        ];

        if ($request->email != NULL) {
            $rules['email'] = "max:50|unique:supplier,email,$supplier->id_supplier,id_supplier|email:dns";
        }

        $request->validate($rules);
        try {
            Log::info('Request update data supplier berisi = ' . json_encode($supplier));
            Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
            Log::info('Start');

            DB::beginTransaction();

            $supplier->update([
                'nama' => strtoupper($request->nama),
                'notelp' => $request->keterangan,
                'email' => $request->email,
                'npwp' => $request->npwp,
            ]);

            Log::info("Data supplier setelah diupdate = " . json_encode($supplier));

            DB::commit();

            return redirect()->route('admin.supplier.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil update supplier']);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error update data supplier = " . $exception->getMessage());
            Log::error("Error update data supplier = " . $exception->getFile());
            Log::error("Error update data supplier = " . $exception->getTraceAsString());

            return redirect()->route('admin.supplier.index')->with(['jenis' => 'error', 'pesan' => 'Gagal update supplier']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        Log::info('Request data supplier yang ingin didelete = ' . json_encode($supplier));
        Log::info("Data User = " . json_encode(auth()->guard('admin')->user()));
        Log::info('Start');

        $supplier->delete();

        Log::info('Data supplier berhasil di delete');

        return redirect()->route('admin.supplier.index')->with(['jenis' => 'success', 'pesan' => 'Berhasil delete supplier']);
    }
}
