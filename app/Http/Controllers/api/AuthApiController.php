<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\LogLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthApiController extends Controller
{
    //

    public function login_admin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        $cek = Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password]);
        if ($cek) {
            LogLogin::create([
                'log_browser' => $request->header('User-Agent'),
                'log_ip_browser' => $request->ip(),
                'is_logged' => 1,
                'id_user' => auth()->guard('admin')->user()->id_admin,
                'session_user' => 1,
            ]);

            return response()->json(
                [
                    'status' => 200,
                    'message' => 'Success',
                ]
            );
        } else {
            LogLogin::create([
                'log_browser' => $request->header('User-Agent'),
                'log_ip_browser' => $request->ip(),
                'is_logged' => 2,
                'id_user' => null,
                'session_user' => 1,
            ]);
            return response()->json(
                [
                    'status' => 403,
                    'message' => 'Gagal',
                    'Username atau Password Salah !'
                ]
            );
        }
    }
}
