<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class KategoriApiController extends Controller
{
    protected $dataView;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Kategori::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            ['nama' => 'required|max:40|unique:kategori,nama',]
        );

        try {
            Log::info('Request simpan kategori = ' . json_encode($request->all()));
            Log::info('Start From Android');

            DB::beginTransaction();
            $kategori =  Kategori::create([
                'nama' => ucfirst($request->nama),
            ]);

            Log::info("Data Kategori Baru = " . json_encode($kategori));

            DB::commit();

            return response()->json(
                [
                    'status' => 200,
                    'message' => 'Success',
                ]
            );
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error simpan data kategori = " . $exception->getMessage());
            Log::error("Error simpan data kategori = " . $exception->getFile());
            Log::error("Error simpan data kategori = " . $exception->getTraceAsString());

            return response()->json(
                [
                    'status' => 403,
                    'message' => 'Gagal',
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json(
            [
                'status' => 200,
                'message' => 'Success',
                'kategori' => Kategori::find($id),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $kategori = Kategori::find($request->id_kategori);

        $request->validate([
            'nama' => "required|max:40|unique:kategori,nama,$kategori->id_kategori,id_kategori",
        ]);

        try {
            Log::info('Request update data kategori berisi = ' . json_encode($kategori));
            Log::info('Start From Android');

            DB::beginTransaction();
            $kategori->update([
                'nama' => ucfirst($request->nama),
            ]);

            Log::info("Data kategori setelah diupdate = " . json_encode($kategori));

            DB::commit();

            return response()->json(
                [
                    'status' => 200,
                    'message' => 'Success',
                ]
            );
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error update data kategori = " . $exception->getMessage());
            Log::error("Error update data kategori = " . $exception->getFile());
            Log::error("Error update data kategori = " . $exception->getTraceAsString());

            return response()->json(
                [
                    'status' => 403,
                    'message' => 'Gagal',
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = Kategori::find($id);
        Log::info('Request data kategori yang ingin didelete = ' . json_encode($kategori));
        Log::info('Start From Android');

        $kategori->delete();

        Log::info('Data kategori berhasil di delete');

        return response()->json(
            [
                'status' => 200,
                'message' => 'Success',
            ]
        );
    }
}
