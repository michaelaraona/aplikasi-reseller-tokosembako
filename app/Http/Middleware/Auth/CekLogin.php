<?php

namespace App\Http\Middleware\Auth;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CekLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // public function handle(Request $request, Closure $next   )
    // {
    //     return $next($request);
    // }

    public function handle(Request $request, Closure $next, ...$roles)
    {

        // dd(Auth::guard('admin')->user());    
        foreach ($roles as $key => $row) {
            if (Auth::guard($row)->user()) {
                if ($row == "admin") {
                    $cek = false;
                    foreach ($roles as $key => $admin) {
                        if (Auth::guard($admin)) {
                            $cek = true;
                        }
                    }

                    if ($cek) {
                        return $next($request);
                    } else {
                        abort(403, 'Anda tidak diberikan ijin akses halaman ini');
                    }
                }
            }
        }

        foreach ($roles as $row) {
            if ($row == 'admin') {
                return redirect()->route('admin.login');
            }
        }
    }
}
