<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;
    protected $table = "produk";
    public $primaryKey = 'id_produk';

    protected $fillable = [
        'nama', 'deskripsi', 'kode_produk', 'supplier_id', 'kategori_id',
    ];

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function stok()
    {
        return $this->hasMany(StokProduk::class);
    }
}
