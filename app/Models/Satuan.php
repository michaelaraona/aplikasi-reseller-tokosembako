<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{
    use HasFactory;
    protected $table = "satuan";
    public $primaryKey = 'id_satuan';

    protected $fillable = [
        'nama', 'keterangan',
    ];

    public function stok()
    {
        return $this->hasMany(StokProduk::class, 'satuan_id');
    }
}
