<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;

    protected $table = 'supplier';
    public $primaryKey = 'id_supplier';
    protected $fillable = [
        'nama', 'email', 'notelp', 'alamat', 'npwp',
    ];

    public function produk()
    {
        return $this->hasMany(Produk::class);
    }
}
