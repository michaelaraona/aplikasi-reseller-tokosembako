<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogLoginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_login', function (Blueprint $table) {
            $table->bigIncrements('id_log_login');
            $table->string('log_browser');
            $table->string('log_ip_browser');
            $table->integer('is_logged')->comment('1 true , 0 false , 2 gagal');
            $table->integer('id_user')->nullable();
            $table->integer('session_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_login');
    }
}
