<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStokProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stokProduk', function (Blueprint $table) {
            $table->bigIncrements('id_stokProduk');
            $table->bigInteger('stok_barang');
            $table->string('image')->nullable();
            $table->foreignId('produk_id');
            $table->foreignId('satuan_id');
            $table->integer('status')->nullable()->comment('0 = bagus, 1 = exp, 2 = rusak');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stok');
    }
}
