<?php

namespace Database\Seeders;

use App\Models\Produk;
use Illuminate\Database\Seeder;

class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Produk::create([
            'kode_produk' => 'SE51N0NA',
            'nama' => 'Pepsodent Center Fresh Pasta Gigi 160G',
            'deskripsi' => 'Pepsodent Center Fresh, pasta gigi pencegah gigi berlubang dengan mouthwash untuk sensasi nafas segar. Pepsodent sebagai salah satu pelopor merek pasta gigi yang mendapatkan sertifikasi halal dari LPPOM MUI dan telah beberapa kali dianugerahi Halal Award.',
            'kategori_id' => 1,
            'supplier_id' => 1,
        ]);
    }
}
