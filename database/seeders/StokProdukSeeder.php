<?php

namespace Database\Seeders;

use App\Models\StokProduk;
use Illuminate\Database\Seeder;

class StokProdukSeeder extends Seeder
{
    // $table->foreignId('produk_id');
    // $table->foreignId('satuan_id');
    // $table->bigInteger('stok_barang');
    // $table->string('image')->nullable();

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StokProduk::create([
            'produk_id' => 1,
            'satuan_id' => 1,
            'stok_barang' => 200,
            'image' => 'pastagigi.jpg',
            'status' => 1
        ]);
    }
}
