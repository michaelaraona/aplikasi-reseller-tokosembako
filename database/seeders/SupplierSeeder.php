<?php

namespace Database\Seeders;

use App\Models\Supplier;
use Illuminate\Database\Seeder;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Supplier::create([
            'nama' => 'PT SAYA ABADI',
            'email' => 'awee@gmail.com',
            'notelp' => '082143843782',
            'alamat' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquid facilis at repellendus porro saepe iusto? Sunt cupiditate eos accusamus. Placeat perspiciatis obcaecati qui cum odio nostrum est ad, corrupti libero.',
            'npwp' => '11.223.345.6-789003',
        ]);
    }
}
