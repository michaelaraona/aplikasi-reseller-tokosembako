@extends('layouts.app')

@section('title')
    Edit Kategori
@endsection

@section('content')
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('admin.kategori.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Edit Kategori Baru</h1>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Kategori</h2>
        <p class="section-lead">
            Halaman ini digunakan untuk mengedit data kategori untuk produk.
        </p>

        <div class="row">
            <div class="col-12">
                <div class="card" id="card">
                    <div class="card-header">
                        <h4>Silahkan edit data kategori.</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.kategori.update', $kategori->id_kategori) }}" method="POST">
                            @csrf
                            @method('put')
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Kategori</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama"
                                        value="{{ $kategori->nama }}">
                                    @error('nama')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    <button class="btn btn-primary" onclick="simpan()">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
