@extends('layouts.app')
@section('title')
    Tambah Produk
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('node_modules/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/selectric/public/selectric.css') }}">
@endpush

@section('content')
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('admin.produk.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Tambah Produk Baru</h1>
    </div>

    <div class="section-body">
        <h2 class="section-title">Tambah Produk</h2>
        <p class="section-lead">
            Halaman ini digunakan untuk menambahkan data produk baru.
        </p>

        <div class="row">
            <div class="col-12">
                <div class="card" id="card">
                    <div class="card-header">
                        <h4>Isikan data produk.</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.produk.store') }}" method="POST">
                            @csrf
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kode Produk</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control @error('kode') is-invalid @enderror" name="kode"
                                        value="{{ old('kode') }}">
                                    @error('kode')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama"
                                        value="{{ old('nama') }}">
                                    @error('nama')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kategori</label>
                                <div class="col-sm-12 col-md-7">
                                    <select class="form-control select2" aria-label="Default select example"
                                        name="kategori_id">
                                        <option value="" selected disabled> --- Pilih Kategori --- </option>
                                        @foreach ($kategori as $k)
                                            <option value="{{ $k->id_kategori }}"
                                                {{ old('kategori_id') == $k->id_kategori ? ' selected' : ' ' }}>
                                                {{ $k->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Supplier</label>
                                <div class="col-sm-12 col-md-7">
                                    <select class="form-control select2" name="supplier_id">
                                        <option value="" selected disabled> --- Pilih Supplier --- </option>
                                        @foreach ($supplier as $sp)
                                            <option style="width:150px" value="{{ $sp->id_supplier }}"
                                                {{ old('supplier_id') == $sp->id_supplier ? ' selected' : ' ' }}>
                                                {{ $sp->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Deskripsi</label>
                                <div class="col-sm-12 col-md-7">
                                    <textarea name="deskripsi" cols="74" rows="9"
                                        style="background-color: #fdfdff; border-color: #e4e6fc;"
                                        class="@error('deskripsi') is-invalid @enderror">{{ old('deskripsi') }}</textarea>
                                    @error('deskripsi')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    <button class="btn btn-primary" onclick="simpan()">Tambah</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('node_modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('node_modules/selectric/public/jquery.selectric.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endpush
