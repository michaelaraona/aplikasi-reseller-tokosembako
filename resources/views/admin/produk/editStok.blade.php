@extends('layouts.app')

@section('title')
    Edit Stok Produk
@endsection

@section('content')
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('admin.produk.show', $produk->id_produk) }}" class="btn btn-icon"><i
                    class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Edit Stok Produk</h1>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Stok Produk</h2>
        <p class="section-lead">
            Halaman ini digunakan untuk mengupdate data stok untuk produk {{ $produk->nama }}.
        </p>

        <div class="row">
            <div class="col-12">
                <div class="card" id="card">
                    <div class="card-header">
                        <h4>Silahkan edit stok baru.</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.produk.updateStok', $stok->id_stokProduk) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="produk_id" value="{{ $produk->id_produk }}">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7" style="max-width: 400px">
                                    <img class="img-preview img-fluid">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Stok</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="number" class="form-control @error('stok') is-invalid @enderror"
                                        name="stok" value="{{ old('stok', $stok->stok_barang) }}">
                                    @error('stok')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Keterangan</label>
                                <div class="col-sm-12 col-md-7">
                                    <textarea name="keterangan" cols="74" rows="9"
                                        style="background-color: #fdfdff; border-color: #e4e6fc;"
                                        class="@error('keterangan') is-invalid @enderror">Edit soalnya bosen</textarea>
                                    @error('keterangan')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    <button class="btn btn-primary" onclick="simpan()">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('node_modules/cleave.js/dist/cleave.min.js') }}"></script>
@endpush
