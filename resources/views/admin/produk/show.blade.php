@extends('layouts.app')

@section('title')
    Detail Produk
@endsection

@push('css')
    <style>
        table,
        th,
        td {
            border: 0px;
            border-collapse: collapse;
        }

        .needbottom {
            border-bottom: 1px solid;
            border-color: rgb(240, 231, 231);
        }

        th,
        td {
            padding: 5px;
            text-align: left;
        }

        th {
            width: 200px;
        }

    </style>

    <link rel="stylesheet" href="{{ asset('node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}">
@endpush

@section('content')
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('admin.produk.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail Produk</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h4>Detail data produk</h4>
                    </div>
                    <div class="card-body">
                        <table class="">
                            <tr class=" needbottom">
                            <th>Kode Produk</th>
                            <td>:</td>
                            <td>{{ $produk->kode_produk }}</td>
                            </tr>
                            <tr class="needbottom">
                                <th>Kategori Barang</th>
                                <td>:</td>
                                <td>{{ $produk->kategori->nama }}</td>
                            </tr>
                            <tr class="needbottom">
                                <th>Nama Barang</th>
                                <td>:</td>
                                <td>{{ $produk->nama }}</td>
                            </tr>
                            <tr class="needbottom">
                                <th>Supplier</th>
                                <td>:</td>
                                <td><a href="{{ route('admin.supplier.show', $produk->supplier->id_supplier) }}"
                                        target="blank">{{ $produk->supplier->nama }}</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="stok" data-toggle="tab" href="#stok2" role="tab"
                                    aria-controls="Stok" aria-selected="true">Stok produk & harga</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#profile2" role="tab"
                                    aria-controls="profile" aria-selected="false">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab2" data-toggle="tab" href="#contact2" role="tab"
                                    aria-controls="contact" aria-selected="false">Contact</a>
                            </li>
                        </ul>
                        <div class="tab-content tab-bordered" id="myTab3Content">
                            <div class="tab-pane fade show active" id="stok2" role="tabpanel" aria-labelledby="stok">
                                <div class="table-responsive">
                                    <table id="data" class="table table-striped table-bordered tabled">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="max-width: 50px">
                                                    No
                                                </th>
                                                <th>Satuan</th>
                                                <th>Stok</th>
                                                <th>Harga satuan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($stokProduk as $row)
                                                <tr>
                                                    <td class="text-center">{{ $loop->iteration }}</td>
                                                    <td width="15%">{{ $row->satuan->nama }}</td>
                                                    <td width="15%">{{ $row->stok_barang }}</td>
                                                    <td></td>
                                                    <td>
                                                        <a href="{{ route('admin.produk.editStok', ['stokID' => $row->id_stokProduk, 'produkID' => $produk->id_produk]) }}"
                                                            class="btn btn-info d-inline mr-2">
                                                            <span>Edit Stok</span>
                                                        </a>
                                                        <a href="{{ route('admin.produk.editHarga', ['stokID' => $row->id_stokProduk, 'produkID' => $produk->id_produk]) }}"
                                                            class="btn btn-warning d-inline">
                                                            <span>Edit Harga</span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile2" role="tabpanel" aria-labelledby="profile-tab2">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque eveniet vero excepturi velit
                                nam hic quis. Harum eos voluptatibus sed porro eligendi eum iste, ex alias, recusandae nulla
                                amet tempore, rem rerum laborum omnis quia minima tempora autem illum id et similique
                                delectus laboriosam? Possimus expedita cum, vel sapiente hic doloremque repudiandae sunt
                                ipsum ducimus aliquid! A iure vero quia debitis nihil nulla earum saepe dolorem incidunt
                                tenetur magni quae corrupti modi fuga, similique et ad dolorum, distinctio ratione harum
                                veniam quibusdam mollitia maxime! Dicta a quos blanditiis tenetur labore nam laboriosam
                                nihil ab sequi totam. Expedita dignissimos quaerat hic.
                            </div>
                            <div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact-tab2">
                                Vestibulum imperdiet odio sed neque ultricies, ut dapibus mi maximus. Proin ligula massa,
                                gravida in lacinia efficitur, hendrerit eget mauris. Pellentesque fermentum, sem interdum
                                molestie finibus, nulla diam varius leo, nec varius lectus elit id dolor. Nam malesuada orci
                                non ornare vulputate. Ut ut sollicitudin magna. Vestibulum eget ligula ut ipsum venenatis
                                ultrices. Proin bibendum bibendum augue ut luctus.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('node_modules/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.tabled').dataTable();
        })
    </script>
@endpush
