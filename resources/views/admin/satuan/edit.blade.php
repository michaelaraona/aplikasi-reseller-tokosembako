@extends('layouts.app')

@section('title')
    Edit Satuan
@endsection

@section('content')
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('admin.satuan.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Edit Satuan Baru</h1>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Satuan</h2>
        <p class="section-lead">
            Halaman ini digunakan untuk mengedit data satuan untuk produk.
        </p>

        <div class="row">
            <div class="col-12">
                <div class="card" id="card">
                    <div class="card-header">
                        <h4>Silahkan edit data satuan.</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.satuan.update', $satuan->id_satuan) }}" method="POST">
                            @csrf
                            @method('put')
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama
                                    Satuan</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama"
                                        value="{{ $satuan->nama }}">
                                    @error('nama')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Keterangan</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control @error('keterangan') is-invalid @enderror"
                                        name="keterangan" value="{{ $satuan->keterangan }}">
                                    @error('keterangan')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    <button class="btn btn-primary" onclick="simpan()">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
