@extends('layouts.app')

@section('title')
    Tambah Stok & Harga
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('node_modules/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/selectric/public/selectric.css') }}">
@endpush

@section('content')
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('admin.stokProduk.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Tambah Stok & Harga Produk</h1>
    </div>

    <div class="section-body">
        <h2 class="section-title">Tambah Stok & Harga</h2>
        <p class="section-lead">
            Halaman ini digunakan untuk menambahkan data stok dan harga baru.
        </p>

        <div class="row">
            <div class="col-12">
                <div class="card" id="card">
                    <div class="card-header">
                        <h4>Isikan data stok dan harga baru.</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.stokProduk.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3 @error('produk_id') is-invalid @enderror">Produk</label>
                                <div class="col-sm-12 col-md-7">
                                    <select class="form-control select2" aria-label="Default select example" name="produk"
                                        id="produk" onchange="aktifkan()">
                                        <option value="false" selected disabled> --- Pilih Produk --- </option>
                                        @foreach ($produk as $p)
                                            <option value="{{ $p->id_produk }}"
                                                {{ old('produk') == $p->id_produk ? ' selected' : ' ' }}>
                                                {{ $p->nama }}</option>
                                        @endforeach
                                    </select>
                                    @error('produk_id')
                                        <div class="text-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3 @error('status') is-invalid @enderror">Status
                                    Produk</label>
                                <div class="col-sm-12 col-md-7">
                                    <select class="form-control select2" aria-label="Default select example" name="status"
                                        id="status" disabled>
                                        <option value="" selected disabled> --- Pilih Status --- </option>
                                        @foreach ($status as $st)
                                            <option value="{{ $st['no'] }}"
                                                {{ old('status') == $st['no'] ? ' selected' : ' ' }}>
                                                {{ $st['kondisi'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('status')
                                        <div class="text-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="image"
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3 @error('image') is-invalid @enderror">Gambar
                                    Produk</label>
                                <div class="col-sm-12 col-md-7">
                                    <img class="col-sm-8 img-preview img-fluid mb-2">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" accept="image/*" id="image"
                                            name="image" disabled>
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    @error('image')
                                        <div class="text-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3 @error('stok') is-invalid @enderror">Stok</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="number" class="form-control @error('stok') is-invalid @enderror"
                                        name="stok" id="stok" value="{{ old('stok') }}" disabled>
                                    @error('stok')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3 @error('satuan') is-invalid @enderror">Satuan</label>
                                <div class="col-sm-12 col-md-7">
                                    <select class="form-control select2" aria-label="Default select example" name="satuan"
                                        id="satuan_id" disabled>
                                    </select>
                                    @error('satuan')
                                        <div class="text-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3 @error('harga') is-invalid @enderror">Harga
                                    Produk</label>
                                <div class="input-group col-sm-12 col-md-7">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            IDR
                                        </div>
                                    </div>
                                    <input type="text" name="harga" value="{{ old('harga') }}"
                                        class="form-control currency" id="harga" disabled>
                                </div>
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    @error('harga')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    <button class="btn btn-primary" id="submit" onclick="simpan()" disabled>Tambah</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('node_modules/cleave.js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('node_modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('node_modules/selectric/public/jquery.selectric.min.js') }}"></script>

    <script>
        var select = document.getElementById('produk');
        var produk = select.options[select.selectedIndex].value;

        $(document).ready(function() {
            $('.select2').select2();
            var cleaveC = new Cleave('.currency', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            });

            $('.custom-file-input').on('change', function() {
                let fileName = $(this).val().split('\\').pop();
                $(this).next('.custom-file-label').addClass("selected").html(fileName);
                const image = document.querySelector('#image');
                const imgPreview = document.querySelector('.img-preview');
                imgPreview.style.display = 'block';

                const ofReader = new FileReader();
                ofReader.readAsDataURL(image.files[0]);

                ofReader.onload = function(oFRevent) {
                    imgPreview.src = oFRevent.target.result;
                }
            });

            if (produk != 'false') {
                document.getElementById("status").disabled = false;
                document.getElementById("stok").disabled = false;
                document.getElementById("image").disabled = false;
                document.getElementById("satuan_id").disabled = false;
                document.getElementById("harga").disabled = false;
                document.getElementById("submit").disabled = false;
            }
        });

        function aktifkan() {
            produk = select.options[select.selectedIndex].value;

            $('#satuan_id')
                .find('option')
                .remove();

            $('#satuan_id')
                .find('option')
                .end()
                .append(`<option value="" selected disabled> --- Pilih Satuan ---</option>`)

            $.get(`{{ route('admin.stokProduk.getSatuanByProdukJson', '') }}/${produk}`, function(data) {
                $.each(data, function(index, row) {
                    $('#satuan_id')
                        .find('option')
                        .end()
                        .append(`<option value="${row.id_satuan}">${row.nama}</option>`)
                });
            });

            if (produk != 'false') {
                document.getElementById("status").disabled = false;
                document.getElementById("stok").disabled = false;
                document.getElementById("image").disabled = false;
                document.getElementById("satuan_id").disabled = false;
                document.getElementById("harga").disabled = false;
                document.getElementById("submit").disabled = false;
            }
        }
    </script>
@endpush
