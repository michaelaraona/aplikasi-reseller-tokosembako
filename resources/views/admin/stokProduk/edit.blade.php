@extends('layouts.app')

@section('title')
    Edit Stok & Harga
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('node_modules/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/selectric/public/selectric.css') }}">
@endpush

@section('content')
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('admin.stokProduk.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Edit Stok Produk</h1>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Stok</h2>
        <p class="section-lead">
            Halaman ini digunakan untuk mengupdate data stok.
        </p>

        <div class="row">
            <div class="col-12">
                <div class="card" id="card">
                    <div class="card-header">
                        <h4>Silahkan edit data stok baru.</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.stokProduk.update', $stok->id_stokProduk) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7" style="max-width: 400px;max-height:300px; overflow:hidden">
                                    @if ($stok->image)
                                        <img src="{{ asset('storage/gambar-produk/' . $stok->image) }}"
                                            class="img-preview img-fluid d-block">
                                    @else
                                        <img class="img-preview img-fluid">
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="image"
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3 @error('image') is-invalid @enderror">Gambar
                                    Produk</label>
                                <div class="col-sm-12 col-md-7">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" multiple accept="image/*" id="image"
                                            name="image">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    @error('image')
                                        <div class="text-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Stok</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="number" class="form-control @error('stok') is-invalid @enderror"
                                        name="stok" value="{{ old('stok', $stok->stok_barang) }}">
                                    @error('stok')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Satuan</label>
                                <div class="col-sm-12 col-md-7">
                                    <select class="form-control select2" aria-label="Default select example"
                                        name="satuan_id">
                                        <option value="" selected disabled> --- Pilih Satuan --- </option>
                                        @foreach ($satuan as $s)
                                            <option value="{{ $s->id_satuan }}"
                                                {{ old('satuan_id', $stok->satuan_id) == $s->id_satuan ? ' selected' : ' ' }}>
                                                {{ $s->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label
                                    class="col-form-label text-md-right col-12 col-md-3 col-lg-3 @error('harga') is-invalid @enderror">Harga
                                    Produk</label>
                                <div class="input-group col-sm-12 col-md-7">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            IDR
                                        </div>
                                    </div>
                                    <input type="text" name="harga" value="{{ old('harga') }}"
                                        class="form-control currency" id="harga">
                                </div>
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    @error('harga')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    <button class="btn btn-primary" onclick="simpan()">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('node_modules/cleave.js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('node_modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('node_modules/selectric/public/jquery.selectric.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('.select2').select2();
            var cleaveC = new Cleave('.currency', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            });
        });

        $('.custom-file-input').on('change', function() {
            let fileName = $(this).val().split('\\').pop();
            $(this).next('.custom-file-label').addClass("selected").html(fileName);
            const image = document.querySelector('#image');
            const imgPreview = document.querySelector('.img-preview');
            imgPreview.style.display = 'block';

            const ofReader = new FileReader();
            ofReader.readAsDataURL(image.files[0]);

            ofReader.onload = function(oFRevent) {
                imgPreview.src = oFRevent.target.result;
            }
        });
    </script>
@endpush
