@extends('layouts.app')
@section('title')
    Stok Produk
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}">
@endpush

@section('content')
    <div class="section-header">
        <h1>Stok Produk</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h4>Data Stok Produk</h4>
                        <div class="card-header-action">
                            <a href="{{ route('admin.stokProduk.create') }}" class="btn btn-primary">Tambah Stok Produk</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered tabled">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="max-width: 50px">
                                            No
                                        </th>
                                        <th>Kode Produk</th>
                                        <th>Nama Produk</th>
                                        <th>Stok</th>
                                        <th>Satuan</th>
                                        <th>Kondisi</th>
                                        <th>Harga</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($stok as $row)
                                        <tr>
                                            <td class="text-center">{{ $loop->iteration }}</td>
                                            <td><a href="{{ route('admin.produk.show', $row->produk->id_produk) }}"
                                                    target="__blank">{{ $row->produk->kode_produk }}</a></td>
                                            <td>{{ $row->produk->nama }}</td>
                                            <td>{{ $row->stok_barang }}</td>
                                            <td>{{ $row->satuan->nama }}</td>
                                            <td>
                                                @if ($row->status == 0)
                                                    Bagus
                                                @elseif ($row->status == 1)
                                                    Expired
                                                @elseif ($row->status == 2)
                                                    Rusak
                                                @endif
                                            </td>
                                            <td></td>
                                            <td>
                                                <a href="{{ route('admin.stokProduk.show', $row->id_stokProduk) }}"
                                                    class="btn btn-info">
                                                    <i class="fas fa-eye"></i>
                                                </a>
                                                <a href="{{ route('admin.stokProduk.edit', $row->id_stokProduk) }}"
                                                    class="btn btn-warning">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                                <form
                                                    action="{{ route('admin.stokProduk.destroy', $row->id_stokProduk) }}"
                                                    method="POST" class="d-inline"
                                                    id="form-delete-{{ $row->id_stokProduk }}">
                                                    @method('delete')
                                                    @csrf
                                                    <button type="button" class="btn btn-danger"
                                                        onclick="hapus({{ $row->id_stokProduk }})">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('node_modules/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.tabled').dataTable();
        })
    </script>
@endpush
