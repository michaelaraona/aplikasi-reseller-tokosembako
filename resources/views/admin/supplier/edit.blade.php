@extends('layouts.app')

@section('title')
    Edit Supplier
@endsection

@section('content')
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('admin.supplier.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Edit Supplier Baru</h1>
    </div>

    <div class="section-body">
        <h2 class="section-title">Edit Supplier</h2>
        <p class="section-lead">
            Halaman ini digunakan untuk mengedit data supplier.
        </p>

        <div class="row">
            <div class="col-12">
                <div class="card" id="card">
                    <div class="card-header">
                        <h4>Silahkan edit data supplier.</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.supplier.update', $supplier->id_supplier) }}" method="POST">
                            @csrf
                            @method('put')
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama"
                                        value="{{ $supplier->nama }}">
                                    @error('nama')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="email" class="form-control @error('email') is-invalid @enderror"
                                        name="email" value="{{ $supplier->email }}">
                                    @error('email')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">No Telp</label>
                                <div class="col-sm-12 col-md-7 input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-phone"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control phone-number" @error('notelp') is-invalid
                                        @enderror" name="notelp" value="{{ $supplier->notelp }}">
                                    @error('notelp')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Alamat</label>
                                <div class="col-sm-12 col-md-7">
                                    <textarea name="alamat" cols="74" rows="9"
                                        style="background-color: #fdfdff; border-color: #e4e6fc;"
                                        class="@error('alamat') is-invalid @enderror">{{ $supplier->alamat }}</textarea>
                                    @error('alamat')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NPWP</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control @error('npwp') is-invalid @enderror" name="npwp"
                                        value="{{ $supplier->npwp }}">
                                    @error('npwp')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    <button class="btn btn-primary" onclick="simpan()">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('node_modules/cleave.js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('node_modules/cleave.js/dist/addons/cleave-phone.id.js') }}"></script>
    <script>
        var cleavePN = new Cleave('.phone-number', {
            phone: true,
            phoneRegionCode: 'id'
        });
    </script>
@endpush
