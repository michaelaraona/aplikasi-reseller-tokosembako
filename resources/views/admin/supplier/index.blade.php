@extends('layouts.app')

@section('title')
    Supplier
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}">
@endpush

@section('content')
    <div class="section-header">
        <h1>Supplier</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h4>Data Supplier</h4>
                        <div class="card-header-action">
                            <a href="{{ route('admin.supplier.create') }}" class="btn btn-primary">Tambah Supplier</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered tabled">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="max-width: 50px">
                                            No
                                        </th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>No Telp</th>
                                        <th>Alamat</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($supplier as $row)
                                        <tr>
                                            <td class="text-center">{{ $loop->iteration }}</td>
                                            <td>{{ $row->nama }}</td>
                                            <td>{{ $row->email ?? '-' }}</td>
                                            <td>{{ $row->notelp ?? '-' }}</td>
                                            <td>{{ Str::limit($row->alamat, 40) ?? '-' }}</td>
                                            <td>
                                                <a href="{{ route('admin.supplier.show', $row->id_supplier) }}"
                                                    class="btn btn-info">
                                                    <i class="fas fa-eye"></i>
                                                </a>
                                                <a href="{{ route('admin.supplier.edit', $row->id_supplier) }}"
                                                    class="btn btn-warning">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                                <form action="{{ route('admin.supplier.destroy', $row->id_supplier) }}"
                                                    method="POST" class="d-inline"
                                                    id="form-delete-{{ $row->id_supplier }}">
                                                    @method('delete')
                                                    @csrf
                                                    <button type="button" class="btn btn-danger"
                                                        onclick="hapus({{ $row->id_supplier }})">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('node_modules/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.tabled').dataTable();
        })
    </script>
@endpush
