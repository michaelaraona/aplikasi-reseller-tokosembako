@extends('layouts.app')

@section('title')
    Detail Supplier
@endsection

@push('css')
    <style>
        table,
        th,
        td {
            border: 0px;
            border-collapse: collapse;
        }

        .needbottom {
            border-bottom: 1px solid;
            border-color: rgb(240, 231, 231);
        }

        th,
        td {
            padding: 5px;
            text-align: left;
        }

        th {
            width: 200px;
        }

    </style>

    <link rel="stylesheet" href="{{ asset('node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}">
@endpush

@section('content')
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('admin.supplier.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail Supplier</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h4>Detail data supplier</h4>
                    </div>
                    <div class="card-body">
                        <table class="">
                            <tr class=" needbottom">
                            <th>Nama</th>
                            <td>:</td>
                            <td>{{ $supplier->nama }}</td>
                            </tr>
                            <tr class="needbottom">
                                <th>Email</th>
                                <td>:</td>
                                <td>{{ $supplier->email }}</td>
                            </tr>
                            <tr class="needbottom">
                                <th>Nomor Telepon</th>
                                <td>:</td>
                                <td>{{ $supplier->notelp }}</td>
                            </tr>
                            <tr class="needbottom">
                                <th>Alamat</th>
                                <td>:</td>
                                <td>{{ $supplier->alamat }}</td>
                            </tr>
                            <tr>
                                <th>NPWP</th>
                                <td>:</td>
                                <td>{{ $supplier->npwp }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h4>Data produk</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered tabled">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="max-width: 50px">
                                            No
                                        </th>
                                        <th>Kode Produk</th>
                                        <th>Kategori</th>
                                        <th>Nama</th>
                                        <th>Deskripsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($produk as $row)
                                        <tr>
                                            <td class="text-center">{{ $loop->iteration }}</td>
                                            <td>{{ $row->kode_produk }}</td>
                                            <td>{{ $row->kategori->nama }}</td>
                                            <td>{{ $row->nama }}</td>
                                            <td>{{ $row->deskripsi }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('node_modules/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.tabled').dataTable();
        })
    </script>
@endpush
