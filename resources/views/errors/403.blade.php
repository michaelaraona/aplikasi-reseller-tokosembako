@extends('errors.appError')

@section('title')
    403 Access Denied !
@endsection
@section('errors')
    <h1>403</h1>
    <div class="page-description">
        {{ $exception->getMessage() }}
    </div>
    <div class="mt-3">
      <a href="{{ url()->previous() }}">Kembali</a>
    </div>
@endsection