<li class="menu-header">Dashboard</li>
<li class="nav-item @if (Request::is('admin/dashboard')) active @endif">
    <a href="{{ route('admin.dashboard.index') }}" class="nav-link"><i
            class="fas fa-fire"></i><span>Dashboard</span></a>
</li>
<li class="menu-header">Data Master</li>
<li class="nav-item @if (Request::is('admin/kategori')) active @endif">
    <a href="{{ route('admin.kategori.index') }}" class="nav-link"><i class="fas fa-list-alt"></i><span>Kategori
            Produk</span></a>
</li>
<li class="nav-item @if (Request::is('admin/satuan')) active @endif">
    <a href="{{ route('admin.satuan.index') }}" class="nav-link"><i
            class="fas fa-balance-scale"></i><span>Satuan</span></a>
</li>
<li class="nav-item @if (Request::is('admin/supplier')) active @endif">
    <a href="{{ route('admin.supplier.index') }}" class="nav-link"><i
            class="fas fa-building"></i><span>Supplier</span></a>
</li>
<li class="nav-item @if (Request::is('admin/produk')) active @endif">
    <a href="{{ route('admin.produk.index') }}" class="nav-link"><i
            class="fas fa-boxes"></i><span>Produk</span></a>
</li>
<li class="nav-item @if (Request::is('admin/stokProduk')) active @endif">
    <a href="{{ route('admin.stokProduk.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Stok
            Produk</span></a>
</li>

<li class="menu-header">Stisla</li>
<li class="nav-item dropdown">
    <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i> <span>Contekan</span></a>
    <ul class="dropdown-menu">
        <li><a class="nav-link" href="forms-advanced-form.html">Advanced Form</a></li>
        <li><a class="nav-link" href="forms-editor.html">Editor</a></li>
        <li><a class="nav-link" href="forms-validation.html">Validation</a></li>
    </ul>
</li>
