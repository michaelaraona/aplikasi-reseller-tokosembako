<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="index.html">AWSEN</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="index.html">AWN</a>
    </div>
    <ul class="sidebar-menu">
        @include('layouts.partials.sidebar-menu')
    </ul>
</aside>
