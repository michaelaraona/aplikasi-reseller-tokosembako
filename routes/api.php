<?php

use App\Http\Controllers\api\AuthApiController;
use App\Http\Controllers\api\KategoriApiController;
use App\Http\Controllers\api\SatuanApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login-admin', [AuthApiController::class, 'login_admin']);

// Kategori
Route::get('get-kategori', [KategoriApiController::class, 'index']);
Route::post('add-kategori', [KategoriApiController::class, 'store']);
Route::get('edit-kategori/{id}', [KategoriApiController::class, 'edit']);
Route::put('update-kategori', [KategoriApiController::class, 'update']);
Route::get('delete-kategori/{id}', [KategoriApiController::class, 'destroy']);

// Satuan
Route::get('get-satuan', [SatuanApiController::class, 'index']);
Route::post('add-satuan', [SatuanApiController::class, 'store']);
Route::get('edit-satuan/{id}', [SatuanApiController::class, 'edit']);
Route::put('update-satuan', [SatuanApiController::class, 'update']);
Route::get('delete-satuan/{id}', [SatuanApiController::class, 'destroy']);

Route::get('/pdfGenerate', function () {
    return response()->json(
        [
            'status' => 200,
            'message' => 'testing'
        ]
    );
});
