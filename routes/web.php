<?php

use App\Http\Controllers\admin\{
    AuthController as AuthControllerAdmin,
    DashboardController as DashboardControllerAdmin,
    KategoriController as KategoriControllerAdmin,
    SatuanController as SatuanControllerAdmin,
    SupplierController as SupplierControllerAdmin,
    ProdukController as ProdukControllerAdmin,
    StokProdukController as StokProdukControllerAdmin,
};

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('clear-all', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    return '<h1>Cache Clear</h1>';
});


Route::get('/', function () {
    return redirect()->route('admin.login');
});
Route::get('/pass', function () {
    echo Hash::make("admin");
});


// Auth Admin
Route::get('/login-admin', [AuthControllerAdmin::class, 'index'])->name('admin.login');
Route::post('/login-admin', [AuthControllerAdmin::class, 'store'])->name('admin.login.store');

Route::middleware(['role:admin'])->prefix('admin')->name('admin.')->group(function () {
    Route::get('/logout', [AuthControllerAdmin::class, 'logout'])->name('logout');
    Route::resource('/dashboard', DashboardControllerAdmin::class);
    Route::resource('/kategori', KategoriControllerAdmin::class);
    Route::resource('/satuan', SatuanControllerAdmin::class);
    Route::resource('/supplier', SupplierControllerAdmin::class);

    //group route produk
    Route::resource('produk', ProdukControllerAdmin::class);
    Route::get('produk/editStok/{id}', [ProdukControllerAdmin::class, 'editStok'])->name('produk.editStok');
    Route::put('produk/updateStok/{id}', [ProdukControllerAdmin::class, 'updateStok'])->name('produk.updateStok');
    Route::get('produk/editStok/{stokID}/{produkID}', [ProdukControllerAdmin::class, 'editStok'])->name('produk.editStok');
    Route::get('produk/editHarga/{stokID}/{produkID}', [ProdukControllerAdmin::class, 'editHarga'])->name('produk.editHarga');
    //end route group produk

    //group route stokProduk
    Route::resource('/stokProduk', StokProdukControllerAdmin::class);
    Route::get('stokProduk/getSatuanByProdukJson/{idProduk}', [StokProdukControllerAdmin::class, 'getSatuanByProdukJson'])->name('stokProduk.getSatuanByProdukJson');
    //end route group stokProduk
});
